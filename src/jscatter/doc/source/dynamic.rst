dynamic
=======

.. automodule:: jscatter.dynamic
    :noindex:

Transform between domains
-------------------------
.. autosummary::
    shiftAndBinning
    time2frequencyFF

Time domain
-----------
.. autosummary::
    resolution
    simpleDiffusion
    doubleDiffusion
    cumulantDiff
    cumulant
    cumulantDLS
    finiteRouse
    finiteZimm
    integralZimm
    stretchedExp
    jumpDiffusion
    methylRotation
    diffusionHarmonicPotential
    diffusionPeriodicPotential
    transRotDiffusion
    zilmanGranekBicontinious
    zilmanGranekLamellar

Frequency domain
----------------
.. autosummary::
    h
    hbar
    getHWHM
    convolve
    dynamicSusceptibility
    resolution_w
    elastic_w
    transDiff_w
    jumpDiff_w
    diffusionHarmonicPotential_w
    diffusionInSphere_w
    rotDiffusion_w
    nSiteJumpDiffusion_w


-----

.. automodule:: jscatter.dynamic
    :members:
    :undoc-members:
    :show-inheritance:


