Plotting in XmGrace
===================

.. automodule:: jscatter.graceplot
    :noindex:
        
GracePlot 
---------
.. autosummary::
   GracePlot
   jscatter.grace = GracePlot

------

.. autosummary::
   GracePlot.plot
   GracePlot.multi
   GracePlot.new_graph
   GracePlot.stacked
   GracePlot.clear
   GracePlot.exit 
   GracePlot.legend
   GracePlot.save   
   GracePlot.subtitle
   GracePlot.text
   GracePlot.title
   GracePlot.updateall
   GracePlot.write
   GracePlot.xaxis
   GracePlot.xlabel
   GracePlot.xlimit
   GracePlot.yaxis
   GracePlot.ylabel
   GracePlot.ylimit   
   GracePlot.aspect_scale
   GracePlot.assign_color
   GracePlot.close
   GracePlot.command_args
   GracePlot.focus
   GracePlot.grace_command
   GracePlot.hold
   GracePlot.is_open
   GracePlot.load_parameter_file
   GracePlot.redraw
   GracePlot.resetlast
   GracePlot.resize
   GracePlot.resolution
   GracePlot.send_commands


GraceGraph
----------
.. autosummary::
   GraceGraph.autoscale
   GraceGraph.autotick                                                                                                        
   GraceGraph.clear                                                                                                           
   GraceGraph.frame                                                                                                                                                                                                                  
   GraceGraph.gen_axis                                                                                                        
   GraceGraph.grace                                                                                                           
   GraceGraph.hold                                                                                                            
   GraceGraph.kill                                                                                                            
   GraceGraph.legend                                                                                                          
   GraceGraph.line                                                                                                            
   GraceGraph.plot                                                                                                            
   GraceGraph.redraw                                                                                                          
   GraceGraph.resetlast                                                                                                       
   GraceGraph.subtitle                                                                                                        
   GraceGraph.text                                                                                                            
   GraceGraph.title                                                                                                           
   GraceGraph.update_data                                                                                                     
   GraceGraph.xaxis
   GraceGraph.xlabel
   GraceGraph.xlimit
   GraceGraph.yaxis
   GraceGraph.ylabel
   GraceGraph.ylimit
   GraceGraph.SetView



Helper Classes
--------------
.. autosummary::
      Annotation
      Bar
      Data
      DataBar
      DataXYBoxWhisker
      DataXYDX
      DataXYDXDX
      DataXYDXDXDYDY
      DataXYDXDY
      DataXYDY
      DataXYDYDY
      DataXYZ
      Errorbar
      Label
      Line
      Symbol
      Tick
      TickLabel
      colors
      fills
      frames
      lines
      symbols
      Disconnected
      inheritDocstringFrom
      on_off

-----

.. automodule:: jscatter.graceplot
    :members:
    :undoc-members: 
    :show-inheritance:
