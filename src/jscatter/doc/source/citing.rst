Citing Jscatter
===============

Jscatter paper (use this in a scientific paper)

 Biehl, R. (2019). Jscatter, a program for evaluation and analysis of experimental data.
 PLOS ONE, 14(6), e0218789.
 https://doi.org/10.1371/JOURNAL.PONE.0218789

.. image:: https://zenodo.org/badge/DOI/10.1371/journal.pone.0218789.svg
    :align: left
    :target: https://doi.org/10.1371/journal.pone.0218789


Jscatter latest version at Zenodo (including version archive and history)

 Biehl Ralf. (2019). Jscatter, a Program for Evaluation and Analysis of Experimental Data.
 Zenodo. http://doi.org/10.5281/zenodo.1470306


.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.1470306.svg
    :align: left
    :target: https://doi.org/10.5281/zenodo.1470306

|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|





























