.. Jscatter documentation master file, created by
   sphinx-quickstart on Thu Apr 24 23:56:32 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Jscatter's documentation
========================

.. include:: ../../README.rst
   :end-before:  **Intention and Remarks**

.. automodule:: jscatter

Jscatter package contents
==========================
.. currentmodule::jscatter
    

.. toctree:: 
   :maxdepth: 2
   :numbered:

   BeginnersGuide
   dataArray
   dataList
   formel
   sas
   formfactor
   structurefactor
   dynamic
   bio
   dls
   plotting
   examples
   Extending
   tips
   Remarks
   Installation
   citing
   changelog


.. include:: ../../INSTALLATION.txt
    :start-after: substitutions.txt


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

